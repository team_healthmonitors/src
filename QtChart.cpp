#include "mainwindow.h"
#include <QApplication>
#include <QtCharts>
#include <QtCharts/QLineSeries>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QThread>
#include <QObject>



using namespace QtCharts;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QLineSeries *series = new QLineSeries();
            series->append(0, 50);
            series->append(2, 25);
            series->append(4, 50);
            series->append(6, 75);
            series->append(8, 100);
            series->append(10, 125);
            series->append(12, 150);

            // Create chart, add data, hide legend, and add axis
            QChart *chart = new QChart();
            chart->legend()->hide();
            chart->addSeries(series);
            chart->createDefaultAxes();

            // Customize the title font
            QFont font;
            font.setPixelSize(18);
            chart->setTitleFont(font);
            chart->setTitleBrush(QBrush(Qt::black));
            chart->setTitle("Temperature of Patient");

            // Change the line color and weight
            QPen pen(QRgb(0x000000));
            pen.setWidth(5);
            series->setPen(pen);

            chart->setAnimationOptions(QChart::AllAnimations);

            // Change the x axis categories
            QString work = "time";
            QCategoryAxis *axisX = new QCategoryAxis();
            axisX->append("0",0);
            axisX->append("2",2);
            axisX->append("4",4);
            axisX->append("6",6);
            axisX->append("8",8);
            axisX->append("10",10);
            axisX->append("12",12);
            axisX->setLabelFormat("time");
            axisX->setLabelsVisible();
            chart->setAxisX(axisX, series);

            // Used to display the chart
            QChartView *chartView = new QChartView(chart);
            chartView->setRenderHint(QPainter::Antialiasing);



    //QMainWindow w;
    MainWindow w;
    //work.show();
    w.setCentralWidget(chartView);
    w.resize(420,350);
    w.show();

    return a.exec();
}