import board
import analogio
import time

# Analog in for lm35.
LM35_PIN = board.A0

# Memory for lm35.
adc = analogio.AnalogIn(LM35_PIN)

# Counter
n=0

# Define the size of temporary array for moving Average.
tempp = [0]*8

def Average(dummy):
    return sum(dummy)/ len(dummy)

while True:

    # Convert the ADC value to Voltage
    voltage = adc.value*((adc.reference_voltage*1000)/65535)

    # Calculating Running Average (Window) for Voltage
    tempp[7] = tempp[6]
    tempp[6] = tempp[5]
    tempp[5] = tempp[4]
    tempp[4] = tempp[3]
    tempp[3] = tempp[2]
    tempp[2] = tempp[1]
    tempp[1] = tempp[0]
    tempp[0] = voltage
    voltageAvg = Average(tempp)

    # Calculating Temperature Celsius and Convert to Farhenheit
    temp = (voltageAvg-70)/10
    f = ((9/5)*temp)+32

    # Print Count, Temperature in Celsius, and Temperature in Farhenheit.
    n = n+1
    print('', n)
    print('C == {:.0f}' .format(temp))
    print("F == {:.1f}" .format(f))
    print('\n')
    time.sleep(0.5)