#include "simpletools.h"           // Include simpletools                    

int main()                                  // main function
{

  int Led_Intensity=0;
  pwm_start (1000);
  
  while(1)                                    // Endless loop
  {
    
    while (Led_Intensity<1000)
    {
      pwm_set (0,1,Led_Intensity);
      Led_Intensity ++;
      pause (1);
    }  
    
          
    while (Led_Intensity>0)
    {
      pwm_set (0,1, Led_Intensity);
      Led_Intensity --;
      pause (1);
    }      

  }
}