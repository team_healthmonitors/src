
import QtQuick 2.0

Rectangle { //Lists the characterisitcs of the device itself.
    width: 500
    height: 800

    Header {//This is the header.
        id: header
        anchors.top: parent.top
        headerText: "Characteristics list"
    }

    Dialog {//This is the pop up comment.
        id: info
        anchors.centerIn: parent
        visible: true
        dialogText: "Scanning for characteristics...";
    }

    Connections { //
        target: device
        onCharacteristicsUpdated: {
            menu.menuText = "Back"
            if (characteristicview.count === 0) {
                info.dialogText = "No characteristic found" //Instead of Scanning for characterisitcs this message is displayed.
                info.busyImage = false //Stops the buffer wheel.
            } else {
                info.visible = false
                info.busyImage = true //Plays the buffer wheel.
            }
        }

        onDisconnected: { //if the device is disconnected the the page
            pageLoader.source = "main.qml"
        }
    }

    ListView {
        id: characteristicview
        width: parent.width
        clip: true

        anchors.top: header.bottom
        anchors.bottom: menu.top
        model: device.characteristicList

        delegate: Rectangle {
            id: characteristicbox
            height:300
            width: parent.width
            color: "lightsteelblue"
            border.width: 2
            border.color: "black"
            radius: 5

            Label {
                id: characteristicName
                textContent: modelData.characteristicName
                anchors.top: parent.top
                anchors.topMargin: 5
            }

            Label {
                id: characteristicUuid
                font.pointSize: characteristicName.font.pointSize*0.7
                textContent: modelData.characteristicUuid
                anchors.top: characteristicName.bottom
                anchors.topMargin: 5
            }

            Label {
                id: characteristicValue
                font.pointSize: characteristicName.font.pointSize*0.7
                textContent: ("Value: " + modelData.characteristicValue)
                anchors.bottom: characteristicHandle.top
                horizontalAlignment: Text.AlignHCenter
                anchors.topMargin: 5
            }

            Label {
                id: characteristicHandle
                font.pointSize: characteristicName.font.pointSize*0.7
                textContent: ("Handlers: " + modelData.characteristicHandle)
                anchors.bottom: characteristicPermission.top
                anchors.topMargin: 5
            }

            Label {
                id: characteristicPermission
                font.pointSize: characteristicName.font.pointSize*0.7
                textContent: modelData.characteristicPermission
                anchors.bottom: parent.bottom
                anchors.topMargin: 5
                anchors.bottomMargin: 5
            }
        }
    }

    Menu {
        id: menu
        anchors.bottom: parent.bottom
        menuWidth: parent.width
        menuText: device.update
        menuHeight: (parent.height/6)
        onButtonClick: {
            pageLoader.source = "Services.qml"
            device.update = "Back"
        }
    }
}
