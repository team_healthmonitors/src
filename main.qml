//Bluetooth Low Energy. It creates a snapshot of all services, characteristics and descriptors and presents them to the user.
//Uses Qt Bluetooth Low Energy classes.
import QtQuick 2.0

Rectangle {
    id: back
    width: 600
    height: 600
    property bool deviceState: device.state
    onDeviceStateChanged: {
        if (!device.state)
            info.visible = false; //The info is a pop up screen that displays "Scan for characterisitcs." unless the
        //state has changed, this will be hidden from view until the state has changed.
    }

    Header {
        id: header
        anchors.top: parent.top
        headerText: "SAAG Medical Mointoring" //This is at the top of the app, a header.
    }

    Dialog { //This label controls the info box to be hidden by default.
        id: info
        anchors.centerIn: parent
        visible: false
    }

    ListView {
        id: theListView //The id is called theListView
        width: parent.width //Sets the parent to a global variable defined width.
        clip: true //This auto crop the item depending on the window.

        anchors.top: header.bottom //Stuck to the top, header is stuck to bottom items.
        anchors.bottom: connectToggle.top //Stuck to the bottom, connectToggle is stuck to top items.
        model: device.devicesList //The model is the devices in the list.

        delegate: Rectangle { //Delegate is like setting a template for the devices when they pop up.
            id: box
            height:100
            width: parent.width
            color: "#01b8aa"
            border.width: 2
            border.color: "#01b8aa"
            radius: 5

            Component.onCompleted: { //If devices were able to be seen then the info message will no longer show up
                //and instead the user will be prompted to select a device.
                info.visible = false;
                header.headerText = "Select the patient:";
            }

            MouseArea { //Mouse Area essentially lets the user click on a device and it will load the services.qml program.
                anchors.fill: parent
                onClicked: {
                    device.scanServices(modelData.deviceAddress);
                    pageLoader.source = "Services.qml"
                }
            }

            Label { //devicename is a template for listing the name of the device.
                id: deviceName
                textContent: modelData.deviceName
                anchors.top: parent.top
                anchors.topMargin: 5
            }

            Label { //deviceaddress is a template for listing the device address.
                id: deviceAddress
                textContent: modelData.deviceAddress
                font.pointSize: deviceName.font.pointSize*0.7
                anchors.bottom: box.bottom
                anchors.bottomMargin: 5
            }
        }
    }

    Menu { //not sure what this does
        id: connectToggle

        menuWidth: parent.width
        anchors.bottom: menu.top
        menuText: { if (device.devicesList.length)
                        visible = true
                    else
                        visible = false
                    if (device.useRandomAddress)
                        "Address type: Random"
                    else
                        "Address type: Public"
        }

        onButtonClick: device.useRandomAddress = !device.useRandomAddress;
    }

    Menu {//defines the button clicking on the main menu.
        id: menu
        anchors.bottom: parent.bottom
        menuWidth: parent.width
        menuHeight: (parent.height/6)
        menuText: device.update
        onButtonClick: {
            device.startDeviceDiscovery();
            // if startDeviceDiscovery() failed device.state is not set
            if (device.state) {
                info.dialogText = "Searching...";
                info.visible = true;
            }
        }
    }

    Loader { //This defines the command pageloader which will override everything and fill the parent canvas.
        id: pageLoader
        anchors.fill: parent
    }
}
