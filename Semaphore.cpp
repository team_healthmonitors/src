#include <QtCore>
#include <QtWidgets/QLabel>
#include <stdio.h>
#include <stdlib.h>

/*//! [0]*/
const int DataSize = 20;

const int BufferSize = 20;
int buffer[BufferSize];

QSemaphore freeBytes(BufferSize);
QSemaphore usedBytes;
/*//! [0]

//! [1]*/
class Producer : public QThread
/*//! [1] //! [2]*/
{
public:
    void run() override
    {
        for (int i = 0; i < DataSize; ++i) {
            freeBytes.acquire();
            if (temp == NULL)
                temp=0;
            buffer[i % BufferSize] = i;
            usedBytes.release();
            temp++;
        }
    }
private:
    int temp;
};
/*//! [2]

//! [3]*/
class Consumer : public QThread
/*//! [3] //! [4]*/
{
    Q_OBJECT
public:
    void run() override
    {
        for (int i = 0; i < DataSize; ++i) {
            usedBytes.acquire();
            printf( "%d     ", buffer[i % BufferSize]);
            freeBytes.release();
        }
        fprintf(stderr, "\n");
    }
};
/*//! [4]

//! [5]*/
int main(int argc, char *argv[])
/*//! [5] //! [6]*/
{
    QCoreApplication app(argc, argv);
    //QWidget clone = new QWidget;
    Producer producer;
    Consumer consumer;
    producer.start();
    consumer.start();
    producer.wait();
    consumer.wait();
    return 0;
}
//! [6]

#include "semaphores.moc"
