#ifndef CHANGE_H
#define CHANGE_H
#include <QObject>
#include <QThread>
#include <QtWidgets/QLabel>


class wokerObject : public QObject
{
    Q_OBJECT
    QThread workerThread;

public slots:
    void doWork(QString name)
    {
        QString Last = "Villanueva";
        name = Last;
        emit resultReady(name);
    }
signals:
    void resultReady(QString name);
private:
    const QLabel *newLL = new QLabel;

};



class Change : public QObject
{
    Q_OBJECT
    QThread workerThread;
public:
    Change()
    {

        wokerObject *worker = new wokerObject;
        worker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        connect(this, &Change::operate, worker, &wokerObject::doWork);
        connect(worker, &wokerObject::resultReady, this, &Change::handleResults);


    }
    virtual ~Change()
    {
        workerThread.quit();
        workerThread.wait();
    }
public slots:
    void handleResults(QString name);
signals:
    void operate(QString name);
private:
    QLabel *first = new QLabel;
};

void Change :: handleResults(QString name)
{
    if(name != first->text() )
    {
        first->setText(name);
        //emit operate(name);
    }
}
void Change :: operate(QString newName)
{
    emit  handleResults(newName);
}

#endif // CHANGE_H