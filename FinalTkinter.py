import numpy as np
import tkinter as tk
from tkinter import *

import matplotlib
matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

collector = 'First Name', 'Last Name'
paitent = 'GioVent', 'Warrior'

def write_slogan():
    print("Montering Lives")

root = tk.Tk()
root.wm_title("Embedding in TK")

w = tk.Label(root, justify=LEFT, text="Patient")
w.pack(side=TOP, padx=5)

w.pack()

one = 0
for collector in collector:
  name = Frame(root)
  first = Label(name, width=39, text=collector, anchor='e')
  sirst = Label(name, text=paitent[one])
  one = one+1
  name.pack(side=TOP, fill=X, padx=5, pady=5)
  first.pack(side=LEFT)
  sirst.pack(side=LEFT, padx=10)


frame = tk.Frame(root)
frame.pack()

fig = plt.figure(1)
plt.ion()
t = np.arange(0.0,3.0, 0.02)
s = np.sin(np.pi*t)
plt.plot(t,s)

canvas = FigureCanvasTkAgg(fig, master=root)
plt_widget = canvas.get_tk_widget()
plt_widget.pack()

button = tk.Button (frame, text="Cancel", fg="Red", command=quit)
button.pack(side=tk.RIGHT, pady=10)
slogan = tk.Button (frame, text="Save", fg="Green", command=write_slogan)
slogan.pack(side=tk.RIGHT, pady=10)

root.mainloop()
tkinter.mainloop()